package classes

import scala.util.Random

class LetterRound (){
  val vowelDeck :List[Char] =List('A','A','A','A','A','A','E','E','E','E','E','E','E','I','I','I','I','I','O','O','O','O','O','O','O','U','U','U','U')
  val consonantDeck: List[Char] = List('B', 'B','B', 'B', 'B', 'B',
    'C','C','C','C','C','C', 'D', 'D', 'D', 'D', 'D', 'F', 'F', 'F', 'F', 'G', 'G', 'G', 'G',
    'G', 'G', 'H', 'H', 'H', 'H', 'H','J','J','J','J', 'K', 'K', 'K', 'K', 'L', 'L', 'L', 'L' ,'M','M','M','M', 'N',
    'N', 'N', 'N', 'P', 'P', 'P', 'P', 'Q', 'Q', 'Q', 'Q', 'R', 'R', 'R','S','S','S', 'T', 'T', 'T', 'V', 'V', 'V', 'V','W','W','W','W', 'X', 'X', 'X', 'X', 'Y', 'Y', 'Y','Z','Z','Z','Z' )


  def randomizeDeck(letterDeck:List[Char]): List[Char] = Random.shuffle(letterDeck)

 def getLetterForRound(vowelNumber:Int , consonantsNumber:Int):List[Char] = {
   require(vowelNumber >= 3 && vowelNumber <= 5   )
   val TOTALlETTERS = 9
   randomizeDeck(vowelDeck).take(vowelNumber) ::: randomizeDeck(consonantDeck).take(TOTALlETTERS - vowelNumber)
 }



}


